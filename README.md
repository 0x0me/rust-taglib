# rust-taglib [![Build Status](https://semaphoreci.com/api/v1/projects/bcba4a20-4ca8-40da-936f-cb8028001ede/485986/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-2)
## Rust TagLib Wrapper

This library provides a Rust interface to the [TagLib](https://taglib.github.io) library, an library for
reading and editing meta data from various popular audio files.

## Getting started
Add the dependency to `rust-taglib` to your `Cargo.toml` file:

```toml
[dependencies.rust-taglib]
git = "https://bitbucket.org/0x0me/rust-taglib"
```

Put the following code inside your `main.rs` file:

```rust
extern crate rust_taglib as taglib;

use std::path::Path;

fn main() {

    let file = Path::new("/some/file/reference");
    let meta = taglib::Metadata::new_from_file(file).unwrap();
    println!("Info for: {:?}",file.file_name().unwrap());

    println!("* Artist:\t{}", meta.get_artist().unwrap());
    println!("* Album:\t{}", meta.get_album().unwrap());
    println!("* Tilte:\t{}", meta.get_title().unwrap());
    println!("* Genre:\t{}", meta.get_genre().unwrap());
}

```

To following example shows how to modify a tag:

```rust
extern crate rust_taglib as taglib;

use std::path::Path;

fn main() {
  let file = Path::new("/some/file/reference");

  match Metadata::new_from_file(file) {
          Ok(tag_data) => {
              // check pre conditions
              let title = tag_data.get_title().unwrap();
              println!("Old title is {}", title)

              // execute save
              tag_data.set_title("my changed title");
              tag_data.save();
          }
          Err(e) => panic!(e)
      };
}

```


## License
This program is licensed under the terms of the [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0),
see [https://www.gnu.org/licenses/gpl-3.0](https://www.gnu.org/licenses/gpl-3.0) for
more information.

## CI
* Branch Master: [![Build Status](https://semaphoreci.com/api/v1/projects/bcba4a20-4ca8-40da-936f-cb8028001ede/485986/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-2)
* Branch Develop: [![Build Status](https://semaphoreci.com/api/v1/projects/bcba4a20-4ca8-40da-936f-cb8028001ede/485976/badge.svg)](https://semaphoreci.com/0x0me/rust-taglib-2)