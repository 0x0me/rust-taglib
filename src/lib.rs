// rust-taglib - rust wrapper around taglib
// Copyright (C) 2015 Michael Engelhardt <me@mindcrime-ilab.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//! This library provides a Rust wrapper around the TagLib library.
//!
//! ## Usage
//! A typical use of the library might look something like this:
//!
//! ```no_run
//! extern crate rust_taglib as taglib;
//!
//! use std::path::Path;
//!
//! let meta = taglib::Metadata::new_from_file(Path::new("some_audio.ogg")).unwrap();
//! let artist = meta.get_artist().unwrap();
//! println!("The artist for the file is {}", artist);
//! ```
//!
#![crate_type = "lib"]
#![crate_name = "rust_taglib"]

extern crate rust_taglib_sys;

extern crate libc;

use std::ffi::{CString,CStr};
use std::str;
use std::path::Path;
use rust_taglib_sys::simple as taglib_sys;


/// macro rule to ease conversion from c-style strings into rust-style strings
/// wrap string into a Result
macro_rules! from_cstr {
    ( $x:expr ) => {
        {
            let artist = str::from_utf8(CStr::from_ptr($x).to_bytes());
            match artist {
                Ok(v) => Ok(v.to_string()),
                Err(e) => Err(e)
            }
        }
    };
}

/// structure containing the wrapper to TagLibs meta data information.
pub struct Metadata {
    fd: Option<*mut taglib_sys::TagLib_File>,
    data: *mut taglib_sys::TagLib_Tag,
    audio_properties: *mut taglib_sys::TagLib_AudioProperties,
}

/// main interface to interact with the meta data set.
impl Metadata {
    /// Load meta data from given file
    pub fn new_from_file(file: &Path) -> Result<Metadata, String> {
        /* TODO: add checks if the file exist and if it is readable
         * This feature could come back if the API is stablized. As using Rust 1.0 stable the
         * PathExt struct and its functions are experimental
         *
         * if !file.exists() {
         *    return Err(format!("File does not exists: {:?}.", file.to_str().unwrap()).to_string());
         * }
         */

        unsafe {
            let c_str_path = CString::new(file.to_str().unwrap()).unwrap();
            let fd = taglib_sys::taglib_file_new(c_str_path.as_ptr());

            // check if TagLib thinks everthing was ok, otherwise fail fast.
            // TODO: try to add a more specific error description
            let success = taglib_sys::taglib_file_is_valid(fd);
            if !success {
                // free memory allocated by c library
                taglib_sys::taglib_file_free(fd);
                return Err("Unknown: TagLib says that it was not successful.".to_string());
            }

            let tags = taglib_sys::taglib_file_tag(fd);

            let ap = taglib_sys::taglib_file_audioproperties(fd);

            Ok(Metadata {fd: Some(fd), data: tags, audio_properties: ap})
        }
    }

    pub fn save(&self) -> bool {
        unsafe{
            taglib_sys::taglib_file_save(self.fd.unwrap())
        }
    }

    /// Get album from meta data set
    pub fn get_album(&self) -> Result<String, str::Utf8Error>  {
        unsafe {
            from_cstr!(taglib_sys::taglib_tag_album(self.data))
        }
    }

    /// Set the name of the album
    pub fn set_album(&self, album: &str) {
        let cstr = CString::new(album).unwrap();
        unsafe {
            taglib_sys::taglib_tag_set_album(self.data, cstr.as_ptr());
        }
    }

    /// Get artist from meta data set
    pub fn get_artist(&self) -> Result<String, str::Utf8Error>  {
        unsafe {
            from_cstr!(taglib_sys::taglib_tag_artist(self.data))
        }
    }

    /// Set artists name
    pub fn set_artist(&self, artist: &str) {
        let cstr = CString::new(artist).unwrap();
        unsafe {
            taglib_sys::taglib_tag_set_artist(self.data, cstr.as_ptr());
        }
    }

    /// Get title from meta data set
    pub fn get_title(&self) -> Result<String, str::Utf8Error>  {
        unsafe {
            from_cstr!(taglib_sys::taglib_tag_title(self.data))
        }
    }

    /// Set title
    pub fn set_title(&self, title: &str) {
        let cstr = CString::new(title).unwrap();
        unsafe {
            taglib_sys::taglib_tag_set_title(self.data, cstr.as_ptr());
        }
    }

    /// Get genre from meta data set
    pub fn get_genre(&self) -> Result<String, str::Utf8Error>  {
        unsafe {
            from_cstr!(taglib_sys::taglib_tag_genre(self.data))
        }
    }


    /// Set genre
    pub fn set_genre(&self, genre: &str) {
        let cstr = CString::new(genre).unwrap();
        unsafe {
            taglib_sys::taglib_tag_set_genre(self.data, cstr.as_ptr());
        }
    }

    /// Get comment from meta data set
    pub fn get_comment(&self) -> Result<String, str::Utf8Error>  {
        unsafe {
            from_cstr!(taglib_sys::taglib_tag_comment(self.data))
        }
    }

    /// Set comment name
    pub fn set_comment(&self, comment: &str) {
        let cstr = CString::new(comment).unwrap();
        unsafe {
            taglib_sys::taglib_tag_set_comment(self.data, cstr.as_ptr());
        }
    }

    /// Get year from meta data set
    pub fn get_year(&self) -> u32 {
        unsafe {
            taglib_sys::taglib_tag_year(self.data)
        }
    }


    /// Set year name
    pub fn set_year(&self, year: u32) {
        unsafe {
            taglib_sys::taglib_tag_set_year(self.data, year);
        }
    }


    /// Get year from meta data set
    pub fn get_track(&self) -> u32 {
        unsafe {
            taglib_sys::taglib_tag_track(self.data)
        }
    }

    /// Set track number
    pub fn set_track(&self, track: u32) {
        unsafe {
            taglib_sys::taglib_tag_set_track(self.data, track);
        }
    }

    /// Get length in seconds
    pub fn get_length(&self) -> i32 {
        unsafe {
            taglib_sys::taglib_audioproperties_length(self.audio_properties)
        }
    }

    /// Get bit rate in kb/s
    pub fn get_bitrate(&self) -> i32 {
        unsafe {
            taglib_sys::taglib_audioproperties_bitrate(self.audio_properties)
        }
    }

    /// get sample rate in Hz
    pub fn get_samplerate(&self) -> i32 {
        unsafe {
            taglib_sys::taglib_audioproperties_samplerate(self.audio_properties)
        }
    }

    /// get number of channels
    pub fn get_channels(&self) -> i32 {
        unsafe {
            taglib_sys::taglib_audioproperties_channels(self.audio_properties)
        }
    }
}

/// destructor to free resources
impl Drop for Metadata {
    fn drop(&mut self) {
        unsafe {
            if self.fd.is_some() {
                // free externally allocated strings
                taglib_sys::taglib_tag_free_strings();
                // free externally allocated resources
                taglib_sys::taglib_file_free(self.fd.unwrap());
            }
        }
        self.fd = None;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Tests
///////////////////////////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod test {
    use super::Metadata;
    use std::path::Path;

    #[test]
    fn test_new_from_file() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());
    }


    /// Test for [Issue #1](https://bitbucket.org/0x0me/rust-taglib/issue/1/fix-memory-leak-if-file-could-not-be)
    #[test]
    fn test_file_does_not_exists() {
        match Metadata::new_from_file(Path::new("does_not_exists.ogg")) {
            Ok(_) => panic!("successfully opened a non-existing file"),
            Err(e) => assert_eq!(e,"Unknown: TagLib says that it was not successful.")
            /* TODO: check for better error message if the exists function becomes a stable rust
             * function
             * Err(e) => assert_eq!(e, "File does not exists: \"does_not_exists.ogg\".")
             */
        }
    }
    
    #[test]
    fn test_get_album() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());

        let album = meta.unwrap().get_album();
        assert!(album.is_ok());
        assert_eq!(album.unwrap(),"Rust-taglib Sample Data");
    }

    #[test]
    fn test_get_artist() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());

        let artist = meta.unwrap().get_artist();
        assert!(artist.is_ok());
        assert_eq!(artist.unwrap(),"The Audacity Noise Generator");
    }


    #[test]
    fn test_get_title() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());

        let title = meta.unwrap().get_title();
        assert!(title.is_ok());
        assert_eq!(title.unwrap(),"Noise Generated By Audacity");
    }

    #[test]
    fn test_get_genre() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());

        let genre = meta.unwrap().get_genre();
        assert!(genre.is_ok());
        assert_eq!(genre.unwrap(),"Electro");
    }

    #[test]
    fn test_write_tags() {
        use std::fs;
        let test_data_file = "resources/tmp_test_data.ogg";

        let old_album = "Rust-taglib Sample Data";
        let new_album = "new album";

        let old_artist = "The Audacity Noise Generator";
        let new_artist = "new arist";

        let old_title = "Noise Generated By Audacity";
        let new_title = "new title";

        // little closure to remove the temporary test data file
        let fn_clean_up = || {
            match fs::remove_file(test_data_file) {
                Ok(_) => (),
                Err(e) => panic!(e)
            };
        };

        match fs::copy("resources/test_data.ogg", test_data_file) {
            Ok(_) =>  {
                match Metadata::new_from_file(Path::new(test_data_file)) {
                    Ok(tag_data) => {
                        // check pre conditions
                        let album = tag_data.get_album();
                        assert!(album.is_ok());
                        assert_eq!(album.unwrap(), old_album);

                        let artist = tag_data.get_artist();
                        assert!(artist.is_ok());
                        assert_eq!(artist.unwrap(), old_artist);

                        let title = tag_data.get_title();
                        assert!(title.is_ok());
                        assert_eq!(title.unwrap(), old_title);

                        // execute save
                        tag_data.set_artist(new_artist);
                        tag_data.set_album(new_album);
                        tag_data.set_title(new_title);
                        tag_data.save();

                    }
                    Err(e) => {
                        fn_clean_up();
                        panic!(e);
                    }

                };
            }
            Err(e) => panic!(e)
        };


        let tag_data = match Metadata::new_from_file(Path::new(test_data_file)) {
            Ok(tags) => tags,
            Err(e) => {
                fn_clean_up();
                panic!(e);
            }
        };

        let artist = tag_data.get_artist();
        assert!(artist.is_ok());
        assert_eq!(artist.unwrap(), new_artist);

        let album = tag_data.get_album();
        assert!(album.is_ok());
        assert_eq!(album.unwrap(), new_album);

        let title = tag_data.get_title();
        assert!(title.is_ok());
        assert_eq!(title.unwrap(), new_title);

        fn_clean_up();

    }

    #[test]
    fn test_get_length() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());
        let length = meta.unwrap().get_length();
        assert_eq!(length,1);
    }

    #[test]
    fn test_get_bitrate() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());
        let bitrate = meta.unwrap().get_bitrate();
        assert_eq!(bitrate,96);
    }

    #[test]
    fn test_get_samplerate() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());
        let samplerate = meta.unwrap().get_samplerate();
        assert_eq!(samplerate,44100);
    }

    #[test]
    fn test_get_channels() {
        let meta = Metadata::new_from_file(Path::new("resources/test_data.ogg"));
        assert!(meta.is_ok());
        let channels = meta.unwrap().get_channels();
        assert_eq!(channels,1);
    }
}
